package com.tora;

import java.util.LinkedList;
import java.util.List;

public class Consumer implements Runnable{

    private final List<Integer> queue;

    public Consumer(List<Integer> queue) {
        this.queue = queue;
    }

    private void consume() throws InterruptedException {
        synchronized (queue){
            while(queue.size() == 0){
                queue.wait();
            }

            System.out.println("Consumed value: " + queue.remove(0));

            queue.notify();
        }
        Thread.sleep(1000);
    }

    @Override
    public void run() {
        while (true){
            try {
                consume();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
