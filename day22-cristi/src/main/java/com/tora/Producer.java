package com.tora;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Producer implements Runnable{

    private final List<Integer> queue;
    private final int maxSize;

    public Producer(List<Integer> queue, int maxSize){
        this.queue = queue;
        this.maxSize = maxSize;
    }

    @Override
    public void run() {
        while (true){
            try {
                Random random = new Random();
                int value = random.nextInt();
                produce(value);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void produce(int i) throws InterruptedException {
        synchronized (queue) {
            if (queue.size() == maxSize) {
                queue.wait();
            }

            queue.add(i);

            queue.notify();

//            Thread.sleep(1000);
        }
    }
}
