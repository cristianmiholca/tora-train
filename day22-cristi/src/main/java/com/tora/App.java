package com.tora;

import java.util.*;

public class App {
    private static final int MAX_SIZE = 1_000_000;
    public static final Object lock = new Object();

    public static void main(String[] args) throws InterruptedException {
        List<Integer> queue = Collections.synchronizedList(new LinkedList<>());

        Thread producer = new Thread(new Producer(queue, MAX_SIZE));
        Thread consumer = new Thread(new Consumer(queue));

        producer.start();
        consumer.start();

        producer.join();
        consumer.join();

    }
}

