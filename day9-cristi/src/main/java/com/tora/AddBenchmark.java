package com.tora;

import org.openjdk.jmh.annotations.*;

import java.util.concurrent.TimeUnit;

public class AddBenchmark {

    @Fork(1)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @BenchmarkMode(Mode.AverageTime)
    @Warmup(iterations = 3, time = 3)
    @Measurement(iterations = 5, time = 5)
    @Benchmark
    public void testJDKArrayListAdd(MyBenchmark.OrderState orderState) {
        orderState.arrayList.add(orderState.order);
    }

    @Fork(1)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @BenchmarkMode(Mode.AverageTime)
    @Warmup(iterations = 3, time = 3)
    @Measurement(iterations = 5, time = 5)
    @Benchmark
    public void testJDKHashSetAdd(MyBenchmark.OrderState orderState) {
        orderState.hashSet.add(orderState.order);
    }

    @Fork(1)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @BenchmarkMode(Mode.AverageTime)
    @Warmup(iterations = 3, time = 3)
    @Measurement(iterations = 5, time = 5)
    @Benchmark
    public void testJDKTreeSetAdd(MyBenchmark.OrderState orderState) {
        orderState.treeSet.add(orderState.order);
    }

    @Fork(1)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @BenchmarkMode(Mode.AverageTime)
    @Warmup(iterations = 3, time = 3)
    @Measurement(iterations = 5, time = 5)
    @Benchmark
    public void testEclipseArrayListAdd(MyBenchmark.OrderState orderState) {
        orderState.eclipseArrayList.add(orderState.order);
    }

    @Fork(1)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @BenchmarkMode(Mode.AverageTime)
    @Warmup(iterations = 3, time = 3)
    @Measurement(iterations = 5, time = 5)
    @Benchmark
    public void testEclipseHashSetAdd(MyBenchmark.OrderState orderState) {
        orderState.eclipseHashSet.add(orderState.order);
    }

    @Fork(1)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @BenchmarkMode(Mode.AverageTime)
    @Warmup(iterations = 3, time = 3)
    @Measurement(iterations = 5, time = 5)
    @Benchmark
    public void testEclipseTreeSetAdd(MyBenchmark.OrderState orderState) {
        orderState.eclipseTreeSet.add(orderState.order);
    }

    @Fork(1)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @BenchmarkMode(Mode.AverageTime)
    @Warmup(iterations = 3, time = 3)
    @Measurement(iterations = 5, time = 5)
    @Benchmark
    public void testFUArrayListAdd(MyBenchmark.OrderState orderState) {
        orderState.fuArrayList.add(orderState.order);
    }

    @Fork(1)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @BenchmarkMode(Mode.AverageTime)
    @Warmup(iterations = 3, time = 3)
    @Measurement(iterations = 5, time = 5)
    @Benchmark
    public void testFUHashSetAdd(MyBenchmark.OrderState orderState) {
        orderState.fuHashSet.add(orderState.order);
    }

    @Fork(1)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @BenchmarkMode(Mode.AverageTime)
    @Warmup(iterations = 3, time = 3)
    @Measurement(iterations = 5, time = 5)
    @Benchmark
    public void testFUTreeSetAdd(MyBenchmark.OrderState orderState) {
        orderState.fuTreeSet.add(orderState.order);
    }

    @Fork(1)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @BenchmarkMode(Mode.AverageTime)
    @Warmup(iterations = 3, time = 3)
    @Measurement(iterations = 5, time = 5)
    @Benchmark
    public void testKolobokeHashSet(MyBenchmark.OrderState orderState) {
        orderState.kolobokeHashSet.add(orderState.order);
    }

    @Fork(1)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @BenchmarkMode(Mode.AverageTime)
    @Warmup(iterations = 3, time = 3)
    @Measurement(iterations = 5, time = 5)
    @Benchmark
    public void testTroveHashSet(MyBenchmark.OrderState orderState) {
        orderState.troveHashSet.add(orderState.order);
    }

}
