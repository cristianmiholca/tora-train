package com.tora.trove4j;

import com.tora.InMemoryRepository;
import gnu.trove.set.hash.THashSet;

import java.util.List;

public class TroveHashSetBasedRepository<T> implements InMemoryRepository<T> {

    private THashSet<T> hashSet;

    public TroveHashSetBasedRepository(){
        this.hashSet = new THashSet<>();
    }

    public TroveHashSetBasedRepository(List<T> list){
        this.hashSet = new THashSet<>(list);
    }

    @Override
    public void add(T elem) {
        hashSet.add(elem);
    }

    @Override
    public boolean contains(T elem) {
        return hashSet.contains(elem);
    }

    @Override
    public void remove(T elem) {
        hashSet.remove(elem);
    }
}
