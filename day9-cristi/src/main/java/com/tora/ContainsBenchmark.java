package com.tora;

import org.openjdk.jmh.annotations.*;

import java.util.concurrent.TimeUnit;

public class ContainsBenchmark {

    @Fork(1)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @BenchmarkMode(Mode.AverageTime)
    @Warmup(iterations = MyBenchmark.WU_ITERATIONS, time = MyBenchmark.WU_TIME)
    @Measurement(iterations = MyBenchmark.MS_ITERATIONS, time = MyBenchmark.MS_TIME)
    @Benchmark
    public void testJDKArrayListContains(MyBenchmark.OrderState orderState){
        orderState.arrayList.contains(orderState.order);
    }

    @Fork(1)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @BenchmarkMode(Mode.AverageTime)
    @Warmup(iterations = MyBenchmark.WU_ITERATIONS, time = MyBenchmark.WU_TIME)
    @Measurement(iterations = MyBenchmark.MS_ITERATIONS, time = MyBenchmark.MS_TIME)
    @Benchmark
    public void testJDKHashSetContains(MyBenchmark.OrderState orderState){
        orderState.hashSet.contains(orderState.order);
    }

    @Fork(1)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @BenchmarkMode(Mode.AverageTime)
    @Warmup(iterations = MyBenchmark.WU_ITERATIONS, time = MyBenchmark.WU_TIME)
    @Measurement(iterations = MyBenchmark.MS_ITERATIONS, time = MyBenchmark.MS_TIME)
    @Benchmark
    public void testJDKTreeSetContains(MyBenchmark.OrderState orderState){
        orderState.treeSet.contains(orderState.order);
    }

    @Fork(1)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @BenchmarkMode(Mode.AverageTime)
    @Warmup(iterations = MyBenchmark.WU_ITERATIONS, time = MyBenchmark.WU_TIME)
    @Measurement(iterations = MyBenchmark.MS_ITERATIONS, time = MyBenchmark.MS_TIME)
    @Benchmark
    public void testEclipseArrayListContains(MyBenchmark.OrderState orderState){
        orderState.eclipseArrayList.contains(orderState.order);
    }

    @Fork(1)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @BenchmarkMode(Mode.AverageTime)
    @Warmup(iterations = MyBenchmark.WU_ITERATIONS, time = MyBenchmark.WU_TIME)
    @Measurement(iterations = MyBenchmark.MS_ITERATIONS, time = MyBenchmark.MS_TIME)
    @Benchmark
    public void testEclipseHashSetContains(MyBenchmark.OrderState orderState){
        orderState.eclipseHashSet.contains(orderState.order);
    }

    @Fork(1)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @BenchmarkMode(Mode.AverageTime)
    @Warmup(iterations = MyBenchmark.WU_ITERATIONS, time = MyBenchmark.WU_TIME)
    @Measurement(iterations = MyBenchmark.MS_ITERATIONS, time = MyBenchmark.MS_TIME)
    @Benchmark
    public void testEclipseTreeSetContains(MyBenchmark.OrderState orderState){
        orderState.eclipseTreeSet.contains(orderState.order);
    }

    @Fork(1)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @BenchmarkMode(Mode.AverageTime)
    @Warmup(iterations = MyBenchmark.WU_ITERATIONS, time = MyBenchmark.WU_TIME)
    @Measurement(iterations = MyBenchmark.MS_ITERATIONS, time = MyBenchmark.MS_TIME)
    @Benchmark
    public void testFUArrayListContains(MyBenchmark.OrderState orderState){
        orderState.fuArrayList.contains(orderState.order);
    }

    @Fork(1)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @BenchmarkMode(Mode.AverageTime)
    @Warmup(iterations = MyBenchmark.WU_ITERATIONS, time = MyBenchmark.WU_TIME)
    @Measurement(iterations = MyBenchmark.MS_ITERATIONS, time = MyBenchmark.MS_TIME)
    @Benchmark
    public void testFUHashSetContains(MyBenchmark.OrderState orderState){
        orderState.fuHashSet.contains(orderState.order);
    }

    @Fork(1)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @BenchmarkMode(Mode.AverageTime)
    @Warmup(iterations = MyBenchmark.WU_ITERATIONS, time = MyBenchmark.WU_TIME)
    @Measurement(iterations = MyBenchmark.MS_ITERATIONS, time = MyBenchmark.MS_TIME)
    @Benchmark
    public void testFUTreeSetContains(MyBenchmark.OrderState orderState) {
        orderState.fuTreeSet.add(orderState.order);
    }

    @Fork(1)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @BenchmarkMode(Mode.AverageTime)
    @Warmup(iterations = MyBenchmark.WU_ITERATIONS, time = MyBenchmark.WU_TIME)
    @Measurement(iterations = MyBenchmark.MS_ITERATIONS, time = MyBenchmark.MS_TIME)
    @Benchmark
    public void testKolobokeHashSetContains(MyBenchmark.OrderState orderState){
        orderState.eclipseArrayList.contains(orderState.order);
    }

    @Fork(1)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @BenchmarkMode(Mode.AverageTime)
    @Warmup(iterations = MyBenchmark.WU_ITERATIONS, time = MyBenchmark.WU_TIME)
    @Measurement(iterations = MyBenchmark.MS_ITERATIONS, time = MyBenchmark.MS_TIME)
    @Benchmark
    public void testTroveHashSetContains(MyBenchmark.OrderState orderState) {
        orderState.troveHashSet.add(orderState.order);
    }

}
