package com.tora.fastutil;

import com.tora.InMemoryRepository;
import it.unimi.dsi.fastutil.objects.ObjectOpenHashSet;
import it.unimi.dsi.fastutil.objects.ObjectSet;

import java.util.List;

public class FUHashSetBasedRepository<T> implements InMemoryRepository<T> {
    private ObjectSet<T> hashSet;

    public FUHashSetBasedRepository(){
        this.hashSet = new ObjectOpenHashSet<>();
    }

    public FUHashSetBasedRepository(List<T> list){
        this.hashSet = new ObjectOpenHashSet<>(list);
    }

    @Override
    public void add(T elem) {
        hashSet.add(elem);
    }

    @Override
    public boolean contains(T elem) {
        return hashSet.contains(elem);
    }

    @Override
    public void remove(T elem) {
        hashSet.remove(elem);
    }
}
