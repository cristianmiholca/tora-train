package com.tora.fastutil;

import com.tora.InMemoryRepository;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import it.unimi.dsi.fastutil.objects.ObjectList;

import java.util.List;

public class FUArrayListBasedRepository<T> implements InMemoryRepository<T> {

    private ObjectList<T> arrayList;

    public FUArrayListBasedRepository(){
        this.arrayList = new ObjectArrayList<>();
    }

    public FUArrayListBasedRepository(List<T> list){
        this.arrayList = new ObjectArrayList<>(list);
    }

    @Override
    public void add(T elem) {
        arrayList.add(elem);
    }

    @Override
    public boolean contains(T elem) {
        return arrayList.contains(elem);
    }

    @Override
    public void remove(T elem) {
        arrayList.remove(elem);
    }
}
