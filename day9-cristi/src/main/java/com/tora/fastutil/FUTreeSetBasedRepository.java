package com.tora.fastutil;

import com.tora.InMemoryRepository;
import it.unimi.dsi.fastutil.objects.ObjectAVLTreeSet;
import it.unimi.dsi.fastutil.objects.ObjectSortedSet;

import java.util.List;

public class FUTreeSetBasedRepository<T> implements InMemoryRepository<T> {

    private ObjectSortedSet<T> treeSet;

    public FUTreeSetBasedRepository(){
        this.treeSet = new ObjectAVLTreeSet<>();
    }

    public FUTreeSetBasedRepository(List<T> list){
        this.treeSet = new ObjectAVLTreeSet<>(list);
    }

    @Override
    public void add(T elem) {
        treeSet.add(elem);
    }

    @Override
    public boolean contains(T elem) {
        return treeSet.contains(elem);
    }

    @Override
    public void remove(T elem) {
        treeSet.remove(elem);
    }
}
