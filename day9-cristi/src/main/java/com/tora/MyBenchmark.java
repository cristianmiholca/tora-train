/*
 * Copyright (c) 2005, 2014, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */

package com.tora;

import com.tora.eclipsecollections.EclipseArrayListBasedRepository;
import com.tora.eclipsecollections.EclipseHashSetBasedRepository;
import com.tora.eclipsecollections.EclipseTreeSetBasedRepository;
import com.tora.fastutil.FUArrayListBasedRepository;
import com.tora.fastutil.FUHashSetBasedRepository;
import com.tora.fastutil.FUTreeSetBasedRepository;
import com.tora.javacollections.JDKArrayListBasedRepository;
import com.tora.javacollections.JDKHashSetBasedRepository;
import com.tora.javacollections.JDKTreeSetBasedRepository;
import com.tora.koloboke.KolobokeHashSetBasedRepository;
import com.tora.trove4j.TroveHashSetBasedRepository;
import org.openjdk.jmh.annotations.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MyBenchmark {

    public static final int WU_ITERATIONS = 3;
    public static final int WU_TIME = 3;
    public static final int MS_ITERATIONS = 5;
    public static final int MS_TIME = 5;

    @State(Scope.Thread)
    public static class OrderState{
        @Param("5000000")
        public static int N;
        public Order order;
        private final List<Order> generatedList = generateList();

        /*JDK Collections*/
        public JDKArrayListBasedRepository<Order> arrayList;
        public JDKHashSetBasedRepository<Order> hashSet;
        public JDKTreeSetBasedRepository<Order> treeSet;

        /*Eclipse Collections*/
        public EclipseArrayListBasedRepository<Order> eclipseArrayList;
        public EclipseHashSetBasedRepository<Order> eclipseHashSet;
        public EclipseTreeSetBasedRepository<Order> eclipseTreeSet;

        /*FastUtil Collection*/
        public FUArrayListBasedRepository<Order> fuArrayList;
        public FUHashSetBasedRepository<Order> fuHashSet;
        public FUTreeSetBasedRepository<Order> fuTreeSet;

        /*Koloboke Collections*/
        public KolobokeHashSetBasedRepository<Order> kolobokeHashSet;

        /*Trove4j Collection*/
        public TroveHashSetBasedRepository<Order> troveHashSet;

        @Setup(Level.Iteration)
        public void setup(){
            Random random = new Random();
            int rand = random.nextInt();
            order = new Order(rand, rand, rand);

            arrayList = new JDKArrayListBasedRepository<>(generatedList);
            hashSet = new JDKHashSetBasedRepository<>(generatedList);
            treeSet = new JDKTreeSetBasedRepository<>(generatedList);
            eclipseArrayList = new EclipseArrayListBasedRepository<>(generatedList);
            eclipseHashSet = new EclipseHashSetBasedRepository<>(generatedList);
            eclipseTreeSet = new EclipseTreeSetBasedRepository<>(generatedList);
            fuArrayList = new FUArrayListBasedRepository<>(generatedList);
            fuHashSet = new FUHashSetBasedRepository<>(generatedList);
            fuTreeSet = new FUTreeSetBasedRepository<>(generatedList);
            kolobokeHashSet = new KolobokeHashSetBasedRepository<>(generatedList);
            troveHashSet = new TroveHashSetBasedRepository<>(generatedList);
        }

        @TearDown(Level.Iteration)
        public void tearDown(){
            arrayList = null;
            hashSet = null;
            treeSet = null;
            eclipseArrayList = null;
            eclipseHashSet = null;
            eclipseTreeSet = null;
            kolobokeHashSet = null;
            troveHashSet = null;
        }
    }

    private static List<Order> generateList(){
        List<Order> list = new ArrayList<>();

        for(int i = 0; i < OrderState.N; i++){
            list.add(new Order(i, i, i));
        }

        return list;
    }

}
