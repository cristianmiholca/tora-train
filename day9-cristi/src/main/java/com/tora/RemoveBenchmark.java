package com.tora;

import org.openjdk.jmh.annotations.*;

import java.util.concurrent.TimeUnit;

public class RemoveBenchmark {

    @Fork(1)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @BenchmarkMode(Mode.AverageTime)
    @Warmup(iterations = MyBenchmark.WU_ITERATIONS, time = MyBenchmark.WU_TIME)
    @Measurement(iterations = MyBenchmark.MS_ITERATIONS, time = MyBenchmark.MS_TIME)
    @Benchmark
    public void testJDKArrayListRemove(MyBenchmark.OrderState orderState){
        orderState.arrayList.remove(orderState.order);
    }

    @Fork(1)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @BenchmarkMode(Mode.AverageTime)
    @Warmup(iterations = MyBenchmark.WU_ITERATIONS, time = MyBenchmark.WU_TIME)
    @Measurement(iterations = MyBenchmark.MS_ITERATIONS, time = MyBenchmark.MS_TIME)
    @Benchmark
    public void testJDKHashSetRemove(MyBenchmark.OrderState orderState){
        orderState.hashSet.remove(orderState.order);
    }

    @Fork(1)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @BenchmarkMode(Mode.AverageTime)
    @Warmup(iterations = MyBenchmark.WU_ITERATIONS, time = MyBenchmark.WU_TIME)
    @Measurement(iterations = MyBenchmark.MS_ITERATIONS, time = MyBenchmark.MS_TIME)
    @Benchmark
    public void testJDKTreeSetRemove(MyBenchmark.OrderState orderState){
        orderState.treeSet.remove(orderState.order);
    }

    @Fork(1)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @BenchmarkMode(Mode.AverageTime)
    @Warmup(iterations = MyBenchmark.WU_ITERATIONS, time = MyBenchmark.WU_TIME)
    @Measurement(iterations = MyBenchmark.MS_ITERATIONS, time = MyBenchmark.MS_TIME)
    @Benchmark
    public void testEclipseArrayListRemove(MyBenchmark.OrderState orderState){
        orderState.eclipseArrayList.remove(orderState.order);
    }

    @Fork(1)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @BenchmarkMode(Mode.AverageTime)
    @Warmup(iterations = MyBenchmark.WU_ITERATIONS, time = MyBenchmark.WU_TIME)
    @Measurement(iterations = MyBenchmark.MS_ITERATIONS, time = MyBenchmark.MS_TIME)
    @Benchmark
    public void testEclipseHashSetRemove(MyBenchmark.OrderState orderState){
        orderState.eclipseHashSet.remove(orderState.order);
    }

    @Fork(1)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @BenchmarkMode(Mode.AverageTime)
    @Warmup(iterations = MyBenchmark.WU_ITERATIONS, time = MyBenchmark.WU_TIME)
    @Measurement(iterations = MyBenchmark.MS_ITERATIONS, time = MyBenchmark.MS_TIME)
    @Benchmark
    public void testEclipseTreeSetRemove(MyBenchmark.OrderState orderState){
        orderState.eclipseTreeSet.remove(orderState.order);
    }

    @Fork(1)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @BenchmarkMode(Mode.AverageTime)
    @Warmup(iterations = MyBenchmark.WU_ITERATIONS, time = MyBenchmark.WU_TIME)
    @Measurement(iterations = MyBenchmark.MS_ITERATIONS, time = MyBenchmark.MS_TIME)
    @Benchmark
    public void testKolobokeHashSetRemove(MyBenchmark.OrderState orderState){
        orderState.kolobokeHashSet.remove(orderState.order);
    }

    @Fork(1)
    @OutputTimeUnit(TimeUnit.MICROSECONDS)
    @BenchmarkMode(Mode.AverageTime)
    @Warmup(iterations = MyBenchmark.WU_ITERATIONS, time = MyBenchmark.WU_TIME)
    @Measurement(iterations = MyBenchmark.MS_ITERATIONS, time = MyBenchmark.MS_TIME)
    @Benchmark
    public void testTroveHashSetRemove(MyBenchmark.OrderState orderState){
        orderState.troveHashSet.remove(orderState.order);
    }

}
