package com.tora.koloboke;

import com.koloboke.collect.set.ObjSet;
import com.koloboke.collect.set.hash.HashObjSets;
import com.tora.InMemoryRepository;

import java.util.List;

public class KolobokeHashSetBasedRepository<T> implements InMemoryRepository<T> {

    private ObjSet<T> hashObjSet;

    public KolobokeHashSetBasedRepository(){
        hashObjSet = HashObjSets.newMutableSet();
    }

    public KolobokeHashSetBasedRepository(List<T> list){
        hashObjSet = HashObjSets.newMutableSet(list);
    }

    @Override
    public void add(T elem) {
        hashObjSet.add(elem);
    }

    @Override
    public boolean contains(T elem) {
        return hashObjSet.contains(elem);
    }

    @Override
    public void remove(T elem) {
        hashObjSet.remove(elem);
    }
}
