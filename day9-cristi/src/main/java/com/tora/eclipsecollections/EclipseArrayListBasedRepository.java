package com.tora.eclipsecollections;

import com.tora.InMemoryRepository;
import org.eclipse.collections.api.list.MutableList;
import org.eclipse.collections.impl.list.mutable.FastList;

import java.util.List;

public class EclipseArrayListBasedRepository<T> implements InMemoryRepository<T> {

    private MutableList<T> fastList;

    public EclipseArrayListBasedRepository(){
        this.fastList = new FastList<>();
    }

    public EclipseArrayListBasedRepository(List<T> list) {
        this.fastList = new FastList<>(list);
    }

    @Override
    public void add(T elem) {
        fastList.add(elem);
    }

    @Override
    public boolean contains(T elem) {
        return fastList.contains(elem);
    }

    @Override
    public void remove(T elem) {
        fastList.remove(elem);
    }
}
