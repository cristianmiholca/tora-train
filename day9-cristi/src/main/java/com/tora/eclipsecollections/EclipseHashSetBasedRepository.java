package com.tora.eclipsecollections;

import com.tora.InMemoryRepository;
import org.eclipse.collections.api.set.MutableSet;
import org.eclipse.collections.impl.set.mutable.UnifiedSet;

import java.util.List;

public class EclipseHashSetBasedRepository<T> implements InMemoryRepository<T> {

    private MutableSet<T> unifiedSet;

    public EclipseHashSetBasedRepository(){
        this.unifiedSet = new UnifiedSet<>();
    }

    public EclipseHashSetBasedRepository(List<T> list){
        this.unifiedSet = new UnifiedSet<>(list);
    }

    @Override
    public void add(T elem) {
        unifiedSet.add(elem);
    }

    @Override
    public boolean contains(T elem) {
        return unifiedSet.contains(elem);
    }

    @Override
    public void remove(T elem) {
        unifiedSet.remove(elem);
    }
}
