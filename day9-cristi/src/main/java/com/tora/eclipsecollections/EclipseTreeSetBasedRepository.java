package com.tora.eclipsecollections;

import com.tora.InMemoryRepository;
import org.eclipse.collections.api.set.sorted.MutableSortedSet;
import org.eclipse.collections.impl.set.sorted.mutable.TreeSortedSet;

import java.util.List;

public class EclipseTreeSetBasedRepository<T> implements InMemoryRepository<T> {

    private MutableSortedSet<T> treeSortedSet;

    public EclipseTreeSetBasedRepository(){
        this.treeSortedSet = new TreeSortedSet<>();
    }

    public EclipseTreeSetBasedRepository(List<T> list){
        this.treeSortedSet = new TreeSortedSet<>(list);
    }

    @Override
    public void add(T elem) {
        treeSortedSet.add(elem);
    }

    @Override
    public boolean contains(T elem) {
        return treeSortedSet.contains(elem);
    }

    @Override
    public void remove(T elem) {
        treeSortedSet.remove(elem);
    }
}
