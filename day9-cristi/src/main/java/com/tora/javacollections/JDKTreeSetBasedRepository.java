package com.tora.javacollections;

import com.tora.InMemoryRepository;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class JDKTreeSetBasedRepository<T> implements InMemoryRepository<T> {
    private Set<T> set;

    public JDKTreeSetBasedRepository(){
        set = new TreeSet<>();
    }

    public JDKTreeSetBasedRepository(List<T> list){
        this.set = new TreeSet<>(list);
    }

    @Override
    public void add(T elem) {
        set.add(elem);
    }

    @Override
    public boolean contains(T elem) {
        return set.contains(elem);
    }

    @Override
    public void remove(T elem) {
        set.remove(elem);
    }
}
