package com.tora.javacollections;

import com.tora.InMemoryRepository;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class JDKHashSetBasedRepository<T> implements InMemoryRepository<T> {

    private Set<T> set;

    public JDKHashSetBasedRepository(){
        set = new HashSet<>();
    }

    public JDKHashSetBasedRepository(List<T> list){
        this.set = new HashSet<>(list);
    }

    @Override
    public void add(T elem) {
        set.add(elem);
    }

    @Override
    public boolean contains(T elem) {
        return set.contains(elem);
    }

    @Override
    public void remove(T elem) {
        set.remove(elem);
    }


}
