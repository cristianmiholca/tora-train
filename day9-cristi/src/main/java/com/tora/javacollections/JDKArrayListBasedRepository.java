package com.tora.javacollections;

import com.tora.InMemoryRepository;

import java.util.ArrayList;
import java.util.List;

public class JDKArrayListBasedRepository<T> implements InMemoryRepository<T> {
    private List<T> arrayList;

    public JDKArrayListBasedRepository(){
        this.arrayList = new ArrayList<>();
    }

    public JDKArrayListBasedRepository(List<T> list){
        this.arrayList = new ArrayList<>(list);
    }

    @Override
    public void add(T elem) {
        arrayList.add(elem);
    }

    @Override
    public boolean contains(T elem) {
        return arrayList.contains(elem);
    }

    @Override
    public void remove(T elem) {
        arrayList.remove(elem);
    }
}
