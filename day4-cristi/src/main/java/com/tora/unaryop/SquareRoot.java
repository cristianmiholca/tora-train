package com.tora.unaryop;

import java.math.BigDecimal;

public class SquareRoot implements IUnaryOperation {

    @Override
    public BigDecimal execute(BigDecimal op) {
        return BigDecimal.valueOf(Math.sqrt(op.doubleValue()));
    }
}
