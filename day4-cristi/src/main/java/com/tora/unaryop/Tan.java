package com.tora.unaryop;

import com.tora.Constants;

import java.math.BigDecimal;

public class Tan implements IUnaryOperation {

    @Override
    public BigDecimal execute(BigDecimal op) {
        double a = op.doubleValue();

        return BigDecimal.valueOf(Math.tan(a)).setScale(Constants.ROUND_ACCURACY, Constants.ROUNDING_MODE);
    }
}
