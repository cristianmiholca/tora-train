package com.tora.unaryop;

import com.tora.Constants;

import java.math.BigDecimal;

public class Round implements IUnaryOperation {

    @Override
    public BigDecimal execute(BigDecimal op) {

        return op.setScale(Constants.ROUND_ACCURACY, Constants.ROUNDING_MODE);
    }
}
