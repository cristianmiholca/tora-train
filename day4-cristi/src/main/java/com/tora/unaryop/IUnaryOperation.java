package com.tora.unaryop;

import com.tora.IOperation;

import java.math.BigDecimal;

public interface IUnaryOperation extends IOperation {

    BigDecimal execute(BigDecimal op);

}
