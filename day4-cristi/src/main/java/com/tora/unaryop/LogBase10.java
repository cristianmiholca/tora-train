package com.tora.unaryop;

import com.tora.Constants;

import java.math.BigDecimal;

public class LogBase10 implements IUnaryOperation {

    @Override
    public BigDecimal execute(BigDecimal op) {
        double a = op.doubleValue();

        return BigDecimal.valueOf(Math.log10(a)).setScale(Constants.ROUND_ACCURACY, Constants.ROUNDING_MODE);
    }
}
