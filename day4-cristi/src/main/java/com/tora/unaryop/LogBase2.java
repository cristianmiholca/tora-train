package com.tora.unaryop;

import com.tora.Constants;

import java.math.BigDecimal;

public class LogBase2 implements IUnaryOperation {
    @Override
    public BigDecimal execute(BigDecimal op) {

        double a = op.doubleValue();
        double result = Math.log10(a) / Math.log10(2);

        return BigDecimal.valueOf(result).setScale(Constants.ROUND_ACCURACY, Constants.ROUNDING_MODE);
    }
}
