package com.tora;

import com.tora.binaryop.*;
import com.tora.unaryop.*;

public class OperationSimpleFactory {

    public static IOperation getOperation(OperationType opType){
        switch (opType) {
            case ADD:
                return new Add();
            case SUBSTRACT:
                return new Substract();
            case MULTIPLY:
                return new Multiply();
            case DIVIDE:
                return new Divide();
            case POW:
                return new Pow();
            case MIN:
                return new Min();
            case MAX:
                return new Max();
            case SQUARE_ROOT:
                return new SquareRoot();
            case COS:
                return new Cos();
            case SIN:
                return new Sin();
            case TAN:
                return new Tan();
            case LOG:
                return new LogBaseE();
            case LOG_BASE_2:
                return new LogBase2();
            case LOG_BASE_10:
                return new LogBase10();
            case ROUND:
                return new Round();
            default:
                System.out.println(Constants.INVALID_OPTION_MESSAGE);
                break;
        }
        return null;
    }

}
