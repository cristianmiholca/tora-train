package com.tora.utils;

import com.tora.Constants;
import com.tora.OperationType;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ReadUtil {

    public static double readParameter() {
        Scanner scanner = new Scanner(System.in);

        while (!scanner.hasNextDouble()) {
            System.out.println(Constants.INVALID_NR_MESSAGE);
            scanner.next();
        }

        return scanner.nextDouble();
    }

    public static OperationType readOperationType() {
        Scanner scanner = new Scanner(System.in);
        int opNumber;
        OperationType operationType;

        do {
            try {
                //Read the operation number
                opNumber = scanner.nextInt();

                //Convert the number to enum type
                operationType = OperationType.values()[opNumber];

                //Return the enum type
                return operationType;
            } catch (IndexOutOfBoundsException | InputMismatchException e) {
                //Print an error message
                System.out.println(Constants.INVALID_OPTION_MESSAGE);
            } finally {
                //Flush the scanner
                scanner.nextLine();
            }
        } while (true);
    }

}
