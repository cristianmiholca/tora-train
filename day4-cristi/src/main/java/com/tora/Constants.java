package com.tora;

import java.math.RoundingMode;

public class Constants {

    public static final String INVALID_NR_MESSAGE = "Please enter a valid number!";
    public static final String FIRST_NR_MESSAGE = "Please enter the first number: ";
    public static final String SECOND_NR_MESSAGE = "Please enter the second number: ";
    public static final String ENTER_OP_MESSAGE = "Please enter one of the above operations: ";
    public static final String RESULT_MESSAGE = "The result is: ";
    public static final String EXIT_MESSAGE = "The app was closed.";
    public static final String INVALID_OPTION_MESSAGE = "Please enter a valid option!";
    public static final String BINARY_OP_EXCEPTION = "There must be two parameters!";
    public static final String UNARY_OP_EXCEPTION = "There must be one parameter!";

    public static final int ROUND_ACCURACY = 4;
    public static final RoundingMode ROUNDING_MODE = RoundingMode.HALF_UP;

}
