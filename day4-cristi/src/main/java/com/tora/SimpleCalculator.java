package com.tora;

import com.tora.utils.ReadUtil;

import java.math.BigDecimal;

public class SimpleCalculator {

    public void run() {
        do {
            System.out.println(createOperationsMenu());

            System.out.println(Constants.ENTER_OP_MESSAGE);
            OperationType operationType = ReadUtil.readOperationType();
            IOperation operation = OperationSimpleFactory.getOperation(operationType);

            if(operation == null){
                break;
            }

            try {
                if (operationType.isBinaryOp()) {
                    executeBinaryOp(operation);
                } else {
                    executeUnaryOp(operation);
                }
            }catch (IllegalArgumentException | ArithmeticException e) {
                //Catch the exceptions and print an explanatory message
                System.out.println(e.getMessage());
            }
        } while (true);

        //Print an info message that the app was closed
        System.out.println(Constants.EXIT_MESSAGE);
    }

    //This is a function which creates tne operations menu automatically based on enum class
    private String createOperationsMenu() {
        StringBuilder opMenu = new StringBuilder(); //StringBuilder object to create the result
        OperationType[] operations = OperationType.values(); //Get the values from the enum class

        for (int i = 0; i < operations.length; i++) {
            OperationType opType = operations[i];
            opMenu.append(i).append(".").append(opType.name()).append("\n");
        }

        return opMenu.toString();
    }

    //The function reads one parameter and prints the result of the unary operation.
    private void executeUnaryOp(IOperation operation) {
        System.out.println(Constants.FIRST_NR_MESSAGE);
        BigDecimal op = BigDecimal.valueOf(ReadUtil.readParameter());

        try {
            System.out.println(Constants.RESULT_MESSAGE + operation.execute(op));
        } catch (IllegalArgumentException | ArithmeticException e) {
            System.out.println(e.getMessage());
        }
    }

    /*The function has one parameter (the operation that is gonna be executed).
      It reads two parameters and prints the result of the operation.*/
    private void executeBinaryOp(IOperation operation) {
        //Read first operand
        System.out.print(Constants.FIRST_NR_MESSAGE);
        BigDecimal op1 = BigDecimal.valueOf(ReadUtil.readParameter());

        //Read second operand
        System.out.print(Constants.SECOND_NR_MESSAGE);
        BigDecimal op2 = BigDecimal.valueOf(ReadUtil.readParameter());

        try {
            //Print the result and a message
            System.out.println(Constants.RESULT_MESSAGE + operation.execute(op1, op2));
        } catch (IllegalArgumentException | ArithmeticException e) {
            //Catch the exceptions and print an explanatory message
            System.out.println(e.getMessage());
        }
    }

}
