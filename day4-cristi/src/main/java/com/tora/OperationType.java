package com.tora;

public enum OperationType {

    EXIT(),
    ADD(true),
    SUBSTRACT(true),
    MULTIPLY(true),
    DIVIDE(true),
    POW(true),
    SQUARE_ROOT(),
    COS(),
    SIN(),
    TAN(),
    LOG(),
    LOG_BASE_2(),
    LOG_BASE_10(),
    MIN(true),
    MAX(true),
    ROUND();

    private final boolean isBinaryOp;

    OperationType(boolean isBinaryOp) {
        this.isBinaryOp = isBinaryOp;
    }

    OperationType() {
        this.isBinaryOp = false;
    }

    public boolean isBinaryOp() {
        return isBinaryOp;
    }
}


