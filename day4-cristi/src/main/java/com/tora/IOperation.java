package com.tora;

import java.math.BigDecimal;

public interface IOperation {

    default BigDecimal execute(BigDecimal op){
        throw new IllegalArgumentException("There must be only one operand!");
    }

    default BigDecimal execute(BigDecimal op1, BigDecimal op2){
        throw new IllegalArgumentException("There must be two operands!");
    }

}
