package com.tora.binaryop;

import java.math.BigDecimal;

public class Add implements IBinaryOperation {

    @Override
    public BigDecimal execute(BigDecimal op1, BigDecimal op2) {
        return op1.add(op2);
    }
}
