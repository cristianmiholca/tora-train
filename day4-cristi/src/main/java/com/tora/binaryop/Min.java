package com.tora.binaryop;

import java.math.BigDecimal;

public class Min implements IBinaryOperation {

    @Override
    public BigDecimal execute(BigDecimal op1, BigDecimal op2) {
        return op1.compareTo(op2) < 0 ? op1 : op2;
    }
}
