package com.tora.binaryop;

import com.tora.Constants;

import java.math.BigDecimal;

public class Pow implements IBinaryOperation {

    @Override
    public BigDecimal execute(BigDecimal op1, BigDecimal op2) {
        double a = op1.doubleValue();
        double b = op2.doubleValue();

        return BigDecimal.valueOf(Math.pow(a, b)).setScale(Constants.ROUND_ACCURACY, Constants.ROUNDING_MODE);
    }
}
