package com.tora.binaryop;

import com.tora.IOperation;

import java.math.BigDecimal;

public interface IBinaryOperation extends IOperation {

    BigDecimal execute(BigDecimal op1, BigDecimal op2);

}
