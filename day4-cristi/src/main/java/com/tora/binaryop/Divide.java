package com.tora.binaryop;

import com.tora.Constants;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Divide implements IBinaryOperation {

    @Override
    public BigDecimal execute(BigDecimal op1, BigDecimal op2) {
        return op1.divide(op2, RoundingMode.CEILING).setScale(Constants.ROUND_ACCURACY, Constants.ROUNDING_MODE);
    }
}
