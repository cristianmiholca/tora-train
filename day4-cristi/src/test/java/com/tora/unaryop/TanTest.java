package com.tora.unaryop;

import com.tora.unaryop.Tan;
import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class TanTest {

    private final Tan tan = new Tan();

    @Test
    public void tan1(){
        assertThat(tan.execute(new BigDecimal(Math.PI / 4)).doubleValue(),
                is(1.0));
    }

    @Test
    public void tan2(){
        assertThat(tan.execute(new BigDecimal(0)).doubleValue(),
                is(0.0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void tan3(){
        tan.execute(new BigDecimal(String.valueOf(Double.POSITIVE_INFINITY)));
    }

}
