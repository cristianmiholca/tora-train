package com.tora.unaryop;

import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class LogBase10Test {

    private final LogBase10 log10 = new LogBase10();

    @Test
    public void log1() {
        assertThat(log10.execute(new BigDecimal(10)).doubleValue(),
                is(1.0));
    }

    @Test
    public void log2() {
        assertThat(log10.execute(new BigDecimal(1000)).doubleValue(),
                is(3.0));
    }

    @Test
    public void log3() {
        assertThat(log10.execute(new BigDecimal("10000000000")).doubleValue(),
                is(10.0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void log4() {
        log10.execute(new BigDecimal(-4));
    }

    @Test(expected = IllegalArgumentException.class)
    public void log5() {
        log10.execute(new BigDecimal(String.valueOf(Double.POSITIVE_INFINITY)));
    }

    @Test
    public void log6() {
        assertThat(log10.execute(new BigDecimal(1)).doubleValue(),
                is(0.0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void log8() {
        log10.execute(new BigDecimal(0));
    }

}
