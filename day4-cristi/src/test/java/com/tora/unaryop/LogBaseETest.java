package com.tora.unaryop;

import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class LogBaseETest {

    private final LogBaseE loge = new LogBaseE();

    @Test
    public void log1() {
        assertThat(loge.execute(new BigDecimal(Math.E)).doubleValue(),
                is(1.0));
    }

    @Test
    public void log2() {
        assertThat(loge.execute(new BigDecimal(Math.E * Math.E)).doubleValue(),
                is(2.0));
    }

    @Test
    public void log3() {
        assertThat(loge.execute(new BigDecimal(1)).doubleValue(),
                is(0.0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void log4() {
        loge.execute(new BigDecimal(-4));
    }

    @Test(expected = IllegalArgumentException.class)
    public void log5() {
        loge.execute(new BigDecimal(String.valueOf(Double.POSITIVE_INFINITY)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void log8() {
        loge.execute(new BigDecimal(0));
    }

}
