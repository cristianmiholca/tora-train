package com.tora.unaryop;

import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class RoundTest {

    private final Round round = new Round();

    @Test
    public void round1(){
        assertThat(round.execute(new BigDecimal("1.36")).doubleValue(),
                    is(1.3600));
    }

    @Test
    public void round2(){
        assertThat(round.execute(new BigDecimal("1.4555")).doubleValue(),
                is(1.4555));
    }

    @Test
    public void round3(){
        assertThat(round.execute(new BigDecimal("1.36789")).doubleValue(),
                is(1.3679));
    }

    @Test
    public void round4(){
        assertThat(round.execute(new BigDecimal("-1.36789")).doubleValue(),
                is(-1.3679));
    }

}
