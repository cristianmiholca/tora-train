package com.tora.unaryop;

import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class CosTest {

    private final Cos cos = new Cos();

    @Test
    public void cos1() {
        assertThat(cos.execute(new BigDecimal(Math.PI)).doubleValue(),
                is(-1.0));
    }

    @Test
    public void cos2() {
        assertThat(cos.execute(new BigDecimal(0)).doubleValue(),
                is(1.0));
    }

    @Test
    public void cos3() {
        assertThat(cos.execute(new BigDecimal(2 * Math.PI)).doubleValue(),
                is(1.0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void cos4() {
        cos.execute(new BigDecimal(String.valueOf(Double.POSITIVE_INFINITY)));
    }

}
