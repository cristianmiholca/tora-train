package com.tora.unaryop;

import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class LogBase2Test {

    private final LogBase2 log2 = new LogBase2();

    @Test
    public void log1() {
        assertThat(log2.execute(new BigDecimal(8)).doubleValue(),
                is(3.0));
    }

    @Test
    public void log2() {
        assertThat(log2.execute(new BigDecimal(1024)).doubleValue(),
                is(10.0));
    }

    @Test
    public void log3() {
        assertThat(log2.execute(new BigDecimal(Integer.MAX_VALUE * 1.0 + 1)).doubleValue(),
                is(31.0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void log4() {
        log2.execute(new BigDecimal(-4));
    }

    @Test(expected = IllegalArgumentException.class)
    public void log5() {
        log2.execute(new BigDecimal(String.valueOf(Double.POSITIVE_INFINITY)));
    }

    @Test
    public void log6() {
        assertThat(log2.execute(new BigDecimal(1)).doubleValue(),
                is(0.0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void log8() {
        log2.execute(new BigDecimal(0));
    }

}
