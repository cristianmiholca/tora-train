package com.tora.unaryop;

import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class SquareRootTest {

    private final SquareRoot sqrt = new SquareRoot();

    @Test
    public void sqrt1() {
        assertThat(sqrt.execute(new BigDecimal(9)).doubleValue(),
                is(3.0));
    }

    @Test
    public void sqrt2() {
        assertThat(sqrt.execute(new BigDecimal(100)).doubleValue(),
                is(10.0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void sqrt3() {
        sqrt.execute(new BigDecimal(-2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void sqrt4() {
        sqrt.execute(new BigDecimal(String.valueOf(Double.POSITIVE_INFINITY)));
    }

}
