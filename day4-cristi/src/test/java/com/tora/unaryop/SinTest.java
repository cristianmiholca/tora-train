package com.tora.unaryop;

import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class SinTest {

    private final Sin sin = new Sin();

    @Test
    public void sin1() {
        assertThat(sin.execute(new BigDecimal(Math.PI / 2)).doubleValue(),
                is(1.0));
    }

    @Test
    public void sin2() {
        assertThat(sin.execute(new BigDecimal(0)).doubleValue(),
                is(0.0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void sin3() {
        sin.execute(new BigDecimal(String.valueOf(Double.POSITIVE_INFINITY)));
    }

}
