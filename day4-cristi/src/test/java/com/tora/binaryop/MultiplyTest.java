package com.tora.binaryop;

import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class MultiplyTest {


    private final Multiply mul = new Multiply();

    @Test
    public void mul1(){
        assertThat(mul.execute(new BigDecimal("2.0"), new BigDecimal("5.0")).doubleValue(),
                is(10.0));
    }

    @Test
    public void mul2(){
        assertThat(mul.execute(new BigDecimal("5"), new BigDecimal("55")).intValue(),
                is(275));    }

    @Test
    public void mul3(){
        assertThat(mul.execute(new BigDecimal(0), new BigDecimal(0)).intValue(),
                is(0));
    }

    @Test
    public void mul4(){
        assertThat(mul.execute(new BigDecimal(5), new BigDecimal(1)).intValue(),
                is(5));
    }
}
