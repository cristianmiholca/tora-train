package com.tora.binaryop;

import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class DivideTest {

    private final Divide div = new Divide();

    @Test(expected = ArithmeticException.class)
    public void divideByZero(){
        div.execute(new BigDecimal(10), new BigDecimal(0));
    }

    @Test
    public void div1(){
        assertThat(div.execute(new BigDecimal("6.0"), new BigDecimal("2.0")).doubleValue(),
                is(3.0));
    }

    @Test
    public void div2(){
        assertThat(div.execute(new BigDecimal("15.0"), new BigDecimal("2.0")).doubleValue(),
                is(7.5));
    }

    @Test
    public void div3(){
        assertThat(div.execute(new BigDecimal("15.0"), new BigDecimal("15.0")).doubleValue(),
                is(1.0));
    }

    @Test
    public void div4(){
        assertThat(div.execute(new BigDecimal(100), new BigDecimal(10)).intValue(),
                is(10));
    }

}
