package com.tora.binaryop;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import java.math.BigDecimal;

public class AddTest {

    private final Add add = new Add();

    @Test
    public void add1(){
        assertThat(add.execute(new BigDecimal("1.23"), new BigDecimal("1.77")).doubleValue(),
                is(3.0));
    }

    @Test
    public void add2(){
        assertThat(add.execute(new BigDecimal("100"), new BigDecimal("15")).intValue(),
                is(115));

    }

    @Test
    public void add3(){
        assertThat(add.execute(new BigDecimal(Integer.MAX_VALUE), new BigDecimal("0")).intValue(),
                is(Integer.MAX_VALUE));
    }

    @Test
    public void add4(){
        assertThat(add.execute(new BigDecimal(1), new BigDecimal("-5")).intValue(),
                is(-4));
    }

    @Test
    public void add5(){
        assertThat(add.execute(new BigDecimal(20), new BigDecimal("-5")).intValue(),
                is(15));
    }

}
