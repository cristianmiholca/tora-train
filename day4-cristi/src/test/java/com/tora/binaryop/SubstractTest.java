package com.tora.binaryop;

import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class SubstractTest {

    private final Substract sub = new Substract();

    @Test
    public void sub1() {
        assertThat(sub.execute(new BigDecimal("3"), new BigDecimal("1")).intValue(),
                is(2));
    }

    @Test
    public void sub2() {
        assertThat(sub.execute(new BigDecimal("3.55"), new BigDecimal("2.31")).doubleValue(),
                is(1.24));
    }

    @Test
    public void sub3() {
        assertThat(sub.execute(new BigDecimal(100), new BigDecimal(-81)).intValue(),
                is(181));
    }

}
