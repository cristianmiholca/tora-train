package com.tora.binaryop;

import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class MinTest {

    private final Min min = new Min();

    @Test
    public void min1(){
        assertThat(min.execute(new BigDecimal(Integer.MAX_VALUE), new BigDecimal("0")).intValue(),
                is(0));
    }

    @Test
    public void min2(){
        assertThat(min.execute(new BigDecimal("1.33"), new BigDecimal("1.34")).doubleValue(),
                is(1.33));
    }

    @Test
    public void min3(){
        assertThat(min.execute(new BigDecimal("0"), new BigDecimal("-1")).intValue(),
                is(-1));
    }

    @Test
    public void min4(){
        assertThat(min.execute(new BigDecimal(Integer.MAX_VALUE), new BigDecimal(Integer.MIN_VALUE)).intValue(),
                is(Integer.MIN_VALUE));
    }

}
