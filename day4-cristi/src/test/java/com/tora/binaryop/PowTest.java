package com.tora.binaryop;

import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class PowTest {

    private final Pow pow = new Pow();

    @Test
    public void pow1() {
        assertThat(pow.execute(new BigDecimal("3"), new BigDecimal("1")).doubleValue(),
                is(3.0));
    }

    @Test
    public void pow2() {
        assertThat(pow.execute(new BigDecimal("0"), new BigDecimal("0")).doubleValue(),
                is(1.0));
    }

    @Test
    public void pow3() {
        assertThat(pow.execute(new BigDecimal("2.76343"), new BigDecimal("0")).doubleValue(),
                is(1.0));
    }

    @Test
    public void pow4() {
        assertThat(pow.execute(new BigDecimal("1"), new BigDecimal("1000")).doubleValue(),
                is(1.0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void pow5() {
        pow.execute(new BigDecimal("2"), new BigDecimal(String.valueOf(Double.NEGATIVE_INFINITY)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void pow6() {
        pow.execute(new BigDecimal(String.valueOf(Double.POSITIVE_INFINITY)),
                new BigDecimal(String.valueOf(Double.POSITIVE_INFINITY)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void pow7() {
        pow.execute(new BigDecimal(String.valueOf(Double.POSITIVE_INFINITY)),
                new BigDecimal(0));
    }

}
