package com.tora;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

public class App {

    public static Stream<String> lines(Path path, Charset charset) throws IOException {
        List<String> arrayList = new ArrayList<>();

        FileInputStream fileInputStream = new FileInputStream(path.toFile());
        BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
        InputStreamReader inputStreamReader = new InputStreamReader(bufferedInputStream, charset);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        while (bufferedReader.ready()) {
            arrayList.add(bufferedReader.readLine());
        }

        bufferedReader.close();
        inputStreamReader.close();
        bufferedInputStream.close();
        fileInputStream.close();

        return arrayList.stream();
    }

    public static Stream<String> lines(Path path) throws IOException {
        return lines(path, StandardCharsets.UTF_8);
    }

    private static void walkHelper(List<Path> arrayList, File[] files) {
        for (File file : files) {
            arrayList.add(Paths.get(file.getPath()));
            if (file.isDirectory()) {
                walkHelper(arrayList, Objects.requireNonNull(file.listFiles()));
            }
        }
    }

    public static Stream<Path> walk(Path path) {
        File[] files = path.toFile().listFiles();
        List<Path> arrayList = new ArrayList<>();
        arrayList.add(path);
        if (files != null) {
            walkHelper(arrayList, files);
        }

        return arrayList.stream();
    }

    public static void main(String[] args) {
//        String path = "day16-cristi/resources/input0.txt";
//        try {
//            lines(path, StandardCharsets.UTF_8)
//                    .forEach(System.out::println);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        Path pathWalk = Paths.get("day16-cristi/src");
        Stream<Path> stringStream = walk(pathWalk);
        stringStream.forEach(System.out::println);

        try{
            Stream<Path> filesStringStream = Files.walk(pathWalk);
            filesStringStream.forEach(System.out::println);
        }catch (IOException e) {
            e.printStackTrace();
        }

    }
}
