package com.tora;

import org.junit.Test;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class AppTest 
{
    public static final Path LINES_FILE_NAME = Paths.get("resources/input0.txt");
    public static final Path PATH = Paths.get("src");

    @Test
    public void testLinesCustom() throws IOException{
        Stream<String> streamCustom = App.lines(LINES_FILE_NAME);

        ArrayList<String> arrayList = new ArrayList<>();
        FileInputStream fileInputStream = new FileInputStream(LINES_FILE_NAME.toFile());
        BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
        InputStreamReader inputStreamReader = new InputStreamReader(bufferedInputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        while (bufferedReader.ready()) {
            arrayList.add(bufferedReader.readLine());
        }

        assertEquals(streamCustom.collect(Collectors.toList()), arrayList);
    }

    @Test
    public void testWalkCustom(){
        List<String> streamList = App.walk(PATH)
                                        .map(Path::toString)
                                        .collect(Collectors.toList());

        List<String> srcDir = Arrays.asList("src",
                "src/test",
                "src/test/java",
                "src/test/java/com",
                "src/test/java/com/tora",
                "src/test/java/com/tora/AppTest.java",
                "src/main",
                "src/main/java",
                "src/main/java/com",
                "src/main/java/com/tora",
                "src/main/java/com/tora/App.java");

        assertEquals(srcDir, streamList);
    }


}
