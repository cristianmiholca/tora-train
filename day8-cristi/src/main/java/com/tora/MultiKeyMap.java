package com.tora;

import java.util.*;

public class MultiKeyMap<Key, V> implements Map<Key, V>{

    private Map<Key, V> hashMap;

    public MultiKeyMap(){
        hashMap = new HashMap<>();
    }

    @Override
    public int size(){
        return hashMap.size();
    }

    @Override
    public boolean isEmpty(){
        return hashMap.size() == 0;
    }

    @Override
    public boolean containsKey(Object key) {
        return hashMap.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return hashMap.containsValue(value);
    }

    @Override
    public V get(Object key) {
        return hashMap.get(key);
    }

    @Override
    public V put(Key key, V value) {
        hashMap.put(key, value);

        return value;
    }

    @Override
    public V remove(Object key) {
        return hashMap.remove(key);
    }

    @Override
    public void putAll(Map<? extends Key, ? extends V> m) {
        hashMap.putAll(m);
    }

    @Override
    public void clear() {
        hashMap.clear();
    }

    @Override
    public Set<Key> keySet() {
        return hashMap.keySet();
    }

    @Override
    public Collection<V> values() {
        return hashMap.values();
    }

    @Override
    public Set<Entry<Key, V>> entrySet() {
        return hashMap.entrySet();
    }

}
