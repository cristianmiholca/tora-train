package com.tora;

import java.util.*;

public class Key<K> {

    private final List<K> key;

    public Key(List<K> keyList) {
        this.key = new ArrayList<>(keyList);
    }

    @SafeVarargs
    public Key(K... keys){
        this.key = new ArrayList<>(Arrays.asList(keys));
    }

    public List<K> getKey(){
        return key;
    }

    @Override
    public boolean equals(Object o){
        if(o == this){
            return true;
        }

        if(o instanceof Key){
            Key<?> key = (Key<?>)o;
            return Objects.equals(this.key, key.getKey());
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(key);
    }

}
