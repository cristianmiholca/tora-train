package com.tora;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class EntryTest {

    @Test
    public void testGetKey(){
        Key<String> key = new Key<>(Arrays.asList("A", "B"));

        assertEquals(key.getKey(), Arrays.asList("A", "B"));
    }

    @Test
    public void testEquals(){
        Key<String> key1 = new Key<>(Arrays.asList("A", "B"));
        Key<String> key2 = new Key<>("A", "B");

        assertEquals(key1, key2);
    }

    @Test
    public void testHashCode(){
        Key<String> key1 = new Key<>(Arrays.asList("A", "B"));
        Key<String> key2 = new Key<>(Arrays.asList("A", "B"));

        assertEquals(key1.hashCode(), key2.hashCode());
    }

}
