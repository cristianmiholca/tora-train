package com.tora;

import org.junit.Test;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertTrue;

public class MultiKeyMapTest {

    @Test
    public void testIsEmpty(){
        MultiKeyMap<Key<String>, Integer> multiKeyMap = new MultiKeyMap<>();

        assertTrue(multiKeyMap.isEmpty());
    }

    @Test
    public void testPut1(){
        MultiKeyMap<Key<String>, Integer> multiKeyMap = new MultiKeyMap<>();

        multiKeyMap.put(new Key<>(Arrays.asList("A", "B")), 5);
        assertThat(multiKeyMap.size(), is(1));
    }

    @Test
    public void testPut2(){
        MultiKeyMap<Key<String>, Integer> multiKeyMap = new MultiKeyMap<>();

        multiKeyMap.put(new Key<>("A", "B"), 5);
        assertThat(multiKeyMap.size(), is(1));
    }

    @Test
    public void testPutOverride(){
        MultiKeyMap<Key<String>, Integer> multiKeyMap = new MultiKeyMap<>();

        multiKeyMap.put(new Key<>("A", "B"), 5);
        multiKeyMap.put(new Key<>("A", "B"), 10);
        assertThat(multiKeyMap.get(new Key<>("A", "B")), is(10));
    }

    @Test
    public void testGet1(){
        MultiKeyMap<Key<String>, Integer> multiKeyMap = new MultiKeyMap<>();
        multiKeyMap.put(new Key<>("A", "B"), 5);
        multiKeyMap.put(new Key<>("A", "C"), 7);
        multiKeyMap.put(new Key<>("A", "A"), 8);

        assertThat(multiKeyMap.get(new Key<>("A", "A")), is(8));
    }

}
