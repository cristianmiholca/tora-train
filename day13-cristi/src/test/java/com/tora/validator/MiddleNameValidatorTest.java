package com.tora.validator;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class MiddleNameValidatorTest {
    private final MiddleNameValidator middleNameValidator = new MiddleNameValidator();

    @Test
    public void testInvalidName(){
        assertFalse(middleNameValidator.validate("abc"));
    }

    @Test
    public void testValidName(){
        assertTrue(middleNameValidator.validate("Ana"));
    }

    @Test
    public void testNoName(){
        assertTrue(middleNameValidator.validate("-"));
    }

    @Test
    public void testEmptyName(){
        assertFalse(middleNameValidator.validate(""));
    }

}
