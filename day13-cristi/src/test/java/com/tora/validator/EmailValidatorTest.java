package com.tora.validator;

import org.junit.Test;

import static org.junit.Assert.*;

public class EmailValidatorTest {

    private final EmailValidator emailValidator = new EmailValidator();

    @Test
    public void testInvalidEmail(){
        assertFalse(emailValidator.validate("cristimiholcagmail.com"));
    }

    @Test
    public void testValidEmail(){
        assertTrue(emailValidator.validate("cristimiholca@gmail.com"));
    }

}
