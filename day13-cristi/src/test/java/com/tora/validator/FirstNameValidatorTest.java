package com.tora.validator;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class FirstNameValidatorTest {

    private final FirstNameValidator firstNameValidator = new FirstNameValidator();

    @Test
    public void testInvalidName(){
        assertFalse(firstNameValidator.validate("abc"));
    }

    @Test
    public void testValidName(){
        assertTrue(firstNameValidator.validate("Ana"));
    }

    @Test
    public void testEmptyName(){
        assertFalse(firstNameValidator.validate(""));
    }

}
