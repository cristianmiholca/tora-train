package com.tora.validator;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

public class ValidatorTest {

    @Test
    public void testValidPerson(){
        assertThat(Validator.validatePerson("A|-|B|1234567890981|AB@gmail.com"), notNullValue());
    }

    @Test
    public void testInvalidMiddleName(){
        assertThat(Validator.validatePerson("A|B|1234567890981|AB@gmail.com"), nullValue());
    }

    @Test
    public void testInvalidFirstName(){
        assertThat(Validator.validatePerson("a|-|B|1234567890981|AB@gmail.com"), nullValue());
    }

    @Test
    public void testInvalidCNP(){
        assertThat(Validator.validatePerson("a|-|B|12345678909|AB@gmail.com"), nullValue());
    }

}
