package com.tora.validator;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class    CNPValidatorTest {

    private final CNPValidator cnpValidator = new CNPValidator();

    @Test
    public void testCNPTooShort(){
        assertFalse(cnpValidator.validate("12345"));
    }

    @Test
    public void testCNPTooLong(){
        assertFalse(cnpValidator.validate("123456789012345"));
    }

    @Test
    public void testCNPgood(){
        assertTrue(cnpValidator.validate("1950228245054"));
    }

}
