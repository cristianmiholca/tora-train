package com.tora.validator;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class LastNameValidatorTest {
    private final LastNameValidator lastNameValidator = new LastNameValidator();

    @Test
    public void testInvalidName(){
        assertFalse(lastNameValidator.validate("abc"));
    }

    @Test
    public void testValidName(){
        assertTrue(lastNameValidator.validate("Ana"));
    }

    @Test
    public void testEmptyName(){
        assertFalse(lastNameValidator.validate(""));
    }
}
