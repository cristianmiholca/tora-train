package com.tora;

import com.tora.business.PersonRepo;
import com.tora.utils.Constants;
import com.tora.utils.InputReader;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class AppTest 
{
    private final String serFileName = "persons.ser";
    private static final String INPUT_0 = "resources/day13_input0.txt";
    private static final String INPUT_1 = "resources/day13_input1.txt";
    private static final String INPUT_2 = "resources/day13_input2.txt";
    private static final String INPUT_3 = "resources/day13_input3.txt";

    @Test
    public void testSerDeser() throws IOException {
        InputReader inputReader = new InputReader(Paths.get(INPUT_0).
                                                                    toAbsolutePath().
                                                                    toString());
        PersonRepo personRepo = new PersonRepo(inputReader.getFileContent());

        App.serAndDeser(personRepo, serFileName);
        assertThat(personRepo.getPersonList().size(), is(7));
        Files.delete(Paths.get(Constants.PERSONS_SER).toAbsolutePath());
    }

    @Test
    public void testSerDeserOneByOne() throws IOException {
        InputReader inputReader = new InputReader(Paths.get(INPUT_0).
                toAbsolutePath().
                toString());
        PersonRepo personRepo = new PersonRepo(inputReader.getFileContent());

        App.serAndDeserOneByOne(personRepo, serFileName);
        assertThat(personRepo.getPersonList().size(), is(7));
        Files.delete(Paths.get(Constants.PERSONS_SER).toAbsolutePath());
    }

    @Test
    public void testSerAndDeserKryo() throws IOException {
        InputReader inputReader = new InputReader(Paths.get(INPUT_0).
                toAbsolutePath().
                toString());
        PersonRepo personRepo = new PersonRepo(inputReader.getFileContent());

        App.serAndDeserKryo(personRepo, serFileName);
        assertThat(personRepo.getPersonList().size(), is(7));
        Files.delete(Paths.get(Constants.PERSONS_SER).toAbsolutePath());
    }
}
