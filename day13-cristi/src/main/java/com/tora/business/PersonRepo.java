package com.tora.business;

import com.google.common.base.Splitter;
import com.tora.validator.Validator;

import java.util.ArrayList;
import java.util.List;

public class PersonRepo {
    private final List<Person> personList;

    public PersonRepo(String fileContent){
        personList = createPersonList(fileContent);
    }

    private List<Person> createPersonList(String input){
        List<Person> personList = new ArrayList<>();
        List<String> personInfo = Splitter.on("#").trimResults().splitToList(input);

        for(String entry : personInfo){
            Person p = Validator.validatePerson(entry);
            if(p != null){
                personList.add(p);
            }
        }

        return personList;
    }

    /*Getters*/
    public List<Person> getPersonList() {
        return personList;
    }

    public void add(Person person){
        personList.add(person);
    }

    public void printList(){
        for(Person p : personList){
            System.out.println(p.toString());
        }
    }

}
