package com.tora.business;

import java.io.Serializable;

public class Person implements Serializable {

    private static final long serialVersionUID = 4L;

    private final String lastName;
    private final String firstName;
    private final String middleName;
    private final String cnp;
    private final String email;

    public Person(String firstName, String middleName, String lastName, String cnp, String email) {
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.cnp = cnp;
        this.email = email;
    }

    public Person() {
        lastName = null;
        firstName = null;
        middleName = null;
        cnp = null;
        email = null;
    }

    public Person(Builder builder){
        this.firstName = builder.firstName;
        this.middleName = builder.middleName;
        this.lastName = builder.lastName;
        this.cnp = builder.cnp;
        this.email = builder.email;
    }

    @Override
    public String toString() {
        return "Person{" +
                "lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", cnp='" + cnp + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    public static class Builder {
        private final String firstName;
        private String middleName;
        private final String lastName;
        private final String cnp;
        private String email;

        public Builder(String firstName, String lastName, String cnp) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.cnp = cnp;
        }

        public Builder setMiddleName(String middleName) {
            this.middleName = middleName;
            return this;
        }

        public Builder setEmail(String email) {
            this.email = email;
            return this;
        }

        public Person build() {
            return new Person(this);
        }
    }

}
