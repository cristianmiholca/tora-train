package com.tora;

import com.tora.business.PersonRepo;
import com.tora.utils.Constants;
import com.tora.utils.InputReader;
import com.tora.utils.SerializationUtils;

import java.io.IOException;

public class App {

    public static void serAndDeser(PersonRepo personRepo, String serFileName){
        try{
            SerializationUtils.serialize(serFileName, personRepo.getPersonList());
            System.out.println("Serialized objects!");
            SerializationUtils.deserialize(serFileName);
            System.out.println("Deserialized objects! Check out the results: persons.ser");
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void serAndDeserOneByOne(PersonRepo personRepo, String serFileName){
        try{
            SerializationUtils.serializeOneByOne(serFileName, personRepo.getPersonList());
            System.out.println("Serialized objects!");
            SerializationUtils.deserializeOneByOne(serFileName);
            System.out.println("Deserialized objects! Check out the results: ");
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void serAndDeserKryo(PersonRepo personRepo, String serFileName){
        try{
            SerializationUtils.kryoSerialize(serFileName, personRepo.getPersonList());
            System.out.println("Serialized objects!");
            SerializationUtils.kryoDeserialize(serFileName);
            System.out.println("Deserialized objects! Check out the results: ");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        //One thread reading from all files
        String[] fileNames = new String[]{Constants.INPUT_0,
                Constants.INPUT_3_2,
                Constants.INPUT_3_1,
                Constants.INPUT_3};

        long t1 = System.currentTimeMillis();
        InputReader inputReader = new InputReader(fileNames, true);
        PersonRepo personRepo = new PersonRepo(inputReader.getFileContent());
        int sum = personRepo.getPersonList().size();
        long t2 = System.currentTimeMillis();
        System.out.println("MultiThreading time: " + (t2 - t1));

        t1 = System.currentTimeMillis();
        inputReader = new InputReader(fileNames, false);
        personRepo = new PersonRepo(inputReader.getFileContent());
        int sumST = personRepo.getPersonList().size();
        t2 = System.currentTimeMillis();
        System.out.println("SingleThread time: " + (t2 - t1));

//        PersonRepo[] repos = new PersonRepo[4];
//        int sumCorrect = 0;
//        for(int i = 0; i < 4; i++){
//            InputReader ir = new InputReader(fileNames[i]);
//            repos[i] = new PersonRepo(ir.getFileContent());
//            sumCorrect += repos[i].getPersonList().size();
//            System.out.println("Repo["+ i + "] size: " + repos[i].getPersonList().size());
//        }
//        System.out.println("Correct sum=" + sumCorrect);
//        System.out.println("Sum=" + sum);
    }
}
