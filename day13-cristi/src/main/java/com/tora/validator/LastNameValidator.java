package com.tora.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LastNameValidator implements IValidate{

    private static final String LAST_NAME_REGEX = "[A-Z][a-z]*";

    @Override
    public boolean validate(String input) {
        Pattern pattern = Pattern.compile(LAST_NAME_REGEX);
        Matcher matcher = pattern.matcher(input);

        return matcher.matches();
    }
}
