package com.tora.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailValidator implements IValidate{

    private static final String EMAIL_REGEX = "^(.+)@(.+)$";

    @Override
    public boolean validate(String input) {
        Pattern pattern = Pattern.compile(EMAIL_REGEX);
        Matcher matcher = pattern.matcher(input);

        return matcher.matches();
    }
}
