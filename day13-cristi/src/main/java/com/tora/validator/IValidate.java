package com.tora.validator;

public interface IValidate {

    boolean validate(String input);

}
