package com.tora.validator;

import com.tora.business.Person;

import java.util.Arrays;
import java.util.List;

public class Validator {

    public static Person validatePerson(String entry){
        List<IValidate> validators = Arrays.asList(
                new FirstNameValidator(),
                new MiddleNameValidator(),
                new LastNameValidator(),
                new CNPValidator(),
                new EmailValidator()
        );

        //infos[0] - firstName; infos[1] - middleName; infos[2] - lastName; infos[3] - cnp
        //infos[4] - email
        String[] infos = entry.split("\\|");
        for(int i = 0 ; i < infos.length; i++){
            IValidate validator = validators.get(i);

            if(!validator.validate(infos[i])){
                return null;
            }
        }

        return new Person.Builder(infos[0], infos[2], infos[3])
                        .setMiddleName(infos[1])
                        .setEmail(infos[4])
                        .build();

    }

}
