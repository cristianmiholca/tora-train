package com.tora.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FirstNameValidator implements IValidate{

    private static final String FIRST_NAME_REGEX = "[A-Z][a-z]*";

    @Override
    public boolean validate(String input) {
        Pattern pattern = Pattern.compile(FIRST_NAME_REGEX);
        Matcher matcher = pattern.matcher(input);

        return matcher.matches();
    }
}
