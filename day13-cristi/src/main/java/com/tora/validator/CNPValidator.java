package com.tora.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CNPValidator implements IValidate{

    private static final String CNP_REGEX = "[0-9]{13}";

    @Override
    public boolean validate(String input) {
        Pattern pattern = Pattern.compile(CNP_REGEX);
        Matcher matcher = pattern.matcher(input);

        return matcher.matches();
    }

}
