package com.tora.utils;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.tora.business.Person;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class SerializationUtils {

    public static void serialize(String toFile, List<Person> personList) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(toFile);
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(bufferedOutputStream);

        objectOutputStream.writeObject(personList);

        objectOutputStream.close();
        bufferedOutputStream.close();
        fileOutputStream.close();
    }

    @SuppressWarnings("unchecked")
    public static List<Person> deserialize(String fromFile) throws IOException, ClassNotFoundException {
        FileInputStream fileInputStream = new FileInputStream(fromFile);
        BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
        ObjectInputStream objectInputStream = new ObjectInputStream(bufferedInputStream);

        List<Person> personList = (ArrayList<Person>)(objectInputStream.readObject());

        objectInputStream.close();
        bufferedInputStream.close();
        fileInputStream.close();

        return personList;
    }

    public static void serializeOneByOne(String toFile, List<Person> personList) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(toFile);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);

        for(Person person : personList){
            objectOutputStream.writeObject(person);
        }

        objectOutputStream.close();
        fileOutputStream.close();
    }

    public static List<Person> deserializeOneByOne(String fromFile) throws IOException, ClassNotFoundException{
        FileInputStream fileInputStream = new FileInputStream(fromFile);
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);

        List<Person> personList = new ArrayList<>();
        Person p;
        while(objectInputStream.available() > 0){
            p = (Person)(objectInputStream.readObject());
            personList.add(p);
        }

        objectInputStream.close();
        fileInputStream.close();

        return personList;
    }

    public static void kryoSerialize(String toFile, List<Person> personList) throws FileNotFoundException {
        Kryo kryo = new Kryo();
        Output output = new Output(new FileOutputStream(toFile));
        kryo.register(ArrayList.class);
        kryo.register(Person.class);
        kryo.writeObject(output, personList);
        output.close();
    }

    @SuppressWarnings("unchecked")
    public static List<Person> kryoDeserialize(String fromFile) throws FileNotFoundException {
        Kryo kryo = new Kryo();
        Input input = new Input(new FileInputStream(fromFile));
        kryo.register(ArrayList.class);
        kryo.register(Person.class);
        return kryo.readObject(input, ArrayList.class);
    }

}
