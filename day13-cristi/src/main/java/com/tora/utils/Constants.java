package com.tora.utils;

public class Constants {

    public static final String INPUT_0 = "day13-cristi/resources/day13_input0.txt";
    public static final String INPUT_1 = "day13-cristi/resources/day13_input1.txt";
    public static final String INPUT_2 = "day13-cristi/resources/day13_input2.txt";
    public static final String INPUT_3 = "day13-cristi/resources/day13_input3.txt";
    public static final String INPUT_3_1 = "day13-cristi/resources/day13_input3_1.txt";
    public static final String INPUT_3_2 = "day13-cristi/resources/day13_input3_2.txt";
    public static final String INPUT_3_3 = "day13-cristi/resources/day13_input3_3.txt";
    public static final String PERSONS_SER = "persons.ser";
}
