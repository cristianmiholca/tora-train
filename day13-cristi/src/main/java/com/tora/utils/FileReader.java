package com.tora.utils;

import java.io.*;

public class FileReader implements Runnable {

    private final String fileName;
    private String fileContent;

    public FileReader(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void run() {

        try {
            FileInputStream fileInputStream = new FileInputStream(fileName);
            BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
            InputStreamReader inputStreamReader = new InputStreamReader(bufferedInputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuilder sb = new StringBuilder();
            while (bufferedReader.ready()) {
                sb.append(bufferedReader.readLine());
            }

            fileContent = sb.toString();
        } catch (IOException e) {
            fileContent = "";
            e.printStackTrace();
        }
    }

    public String getFileContent() {
        return fileContent;
    }

}
