package com.tora.utils;

import java.io.*;

public class MultipleFilesReader implements Runnable{

    private final String[] files;
    private String fileContent;

    public MultipleFilesReader(String[] files) {
        this.files = files;
    }

    public String getFileContent() {
        return fileContent;
    }

    @Override
    public void run() {
        StringBuilder sb = new StringBuilder();
        for(String file : files){
            try {
                FileInputStream fileInputStream = new FileInputStream(file);
                BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
                InputStreamReader inputStreamReader = new InputStreamReader(bufferedInputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                while(bufferedReader.ready()){
                    sb.append(bufferedReader.readLine());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        fileContent = sb.toString();
    }
}
