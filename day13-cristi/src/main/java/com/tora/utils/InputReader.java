package com.tora.utils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class InputReader {

    private String fileContent;

    public InputReader(){
        System.out.println("Please insert the filename: ");

        Scanner scanner = new Scanner(System.in);
        String fileName = scanner.nextLine();

        try {
            fileContent = readFromFile(Paths.get(fileName).toAbsolutePath());
        } catch (IOException e) {
            fileContent = "";
            System.out.println("The file doesn't exist!");
        }

    }

    public InputReader(String fileName){
        try {
            fileContent = readFromFile(Paths.get(fileName).toAbsolutePath());
        } catch (IOException e) {
            fileContent = "";
            System.out.println("The file doesn't exist!");
        }
    }

    public InputReader(String[] fileNames, boolean multiThreading){
        try {
            if(multiThreading){
                readAllFilesMultiThreading(fileNames);
            }else {
                readAllFiles(fileNames);
            }
        } catch (InterruptedException e) {
            fileContent = "";
            System.out.println("Failed reading the files!");
        }
    }

    private void readAllFiles(String[] files) throws InterruptedException {
        MultipleFilesReader runnable = new MultipleFilesReader(files);
        Thread thread = new Thread(runnable);
        thread.start();
        thread.join();

        this.fileContent = runnable.getFileContent();
    }

    private void readAllFilesMultiThreading(String[] files) throws InterruptedException {
        Thread[] threads = new Thread[files.length];
        FileReader[] fileReaders = new FileReader[files.length];
        //create threads
        for(int i = 0; i < files.length; i++){
            fileReaders[i] = new FileReader(files[i]);
            threads[i] = new Thread(fileReaders[i]);
        }

        //start threads
        for (Thread thread : threads) {
            thread.start();
        }

        for (Thread thread : threads) {
            thread.join();
        }

        StringBuilder sb = new StringBuilder();
        //collect results
        for (FileReader fileReader : fileReaders) {
            sb.append(fileReader.getFileContent());
        }

        fileContent = sb.toString();
    }

    private String readFromFile(Path path) throws IOException{
        return new String(Files.readAllBytes(path));
    }

    public String getFileContent() {
        return fileContent;
    }
}
