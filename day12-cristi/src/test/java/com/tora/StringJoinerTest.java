package com.tora;

import org.junit.Test;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class StringJoinerTest {

    @Test
    public void testAddToken(){
        StringJoiner stringJoiner = new StringJoiner();
        stringJoiner.setDelimiter(",");

        stringJoiner.addToken("A");
        stringJoiner.addToken("B");

        assertThat(stringJoiner.getString(), is("A,B"));
    }

    @Test
    public void testRemoveToken(){
        StringJoiner stringJoiner = new StringJoiner();
        stringJoiner.setDelimiter(",");

        stringJoiner.addToken("A");
        stringJoiner.addToken("B");
        stringJoiner.addToken("C");
        stringJoiner.removeToken("B");

        assertThat(stringJoiner.getString(), is("A,C"));
    }


}
