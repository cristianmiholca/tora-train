package com.tora;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class StringTokenizerTest {

    @Test
    public void testCountTokens1(){
        StringTokenizer stringTokenizer = new StringTokenizer();
        stringTokenizer.setString("Ana are mere");
        stringTokenizer.setDelimiter(" ");

        assertThat(stringTokenizer.countTokens(), is(3));
    }

    @Test
    public void testCountTokens2(){
        StringTokenizer stringTokenizer = new StringTokenizer();
        stringTokenizer.setString(" Ana are mere   ");
        stringTokenizer.setDelimiter("        ");

        assertThat(stringTokenizer.countTokens(), is(3));
    }

    @Test
    public void testGetTokens1(){
        StringTokenizer stringTokenizer = new StringTokenizer();
        stringTokenizer.setString("Ana      ]are             [mere");
        stringTokenizer.setDelimiter(" ][");

        String[] expected = new String[]{"Ana", "are", "mere"};
        assertThat(stringTokenizer.getTokens(), is(expected));
    }

    @Test
    public void testGetTokens2(){
        StringTokenizer stringTokenizer = new StringTokenizer();
        stringTokenizer.setString("Ana      ]are             [mere");
        stringTokenizer.setDelimiter("are");

        String[] expected = new String[]{"An", " ]", " [m"};
        assertThat(stringTokenizer.getTokens(), is(expected));
    }

    @Test
    public void testGetTokenAt(){
        StringTokenizer stringTokenizer = new StringTokenizer();
        stringTokenizer.setString(" Ana are ,  mere   ");
        stringTokenizer.setDelimiter("a");

        assertThat(stringTokenizer.getTokenAt(0), is("An"));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testGetTokenAtNegativeIndex(){
        StringTokenizer stringTokenizer = new StringTokenizer();
        stringTokenizer.setString(" Ana are ,  mere   ");
        stringTokenizer.setDelimiter("m");

        assertThat(stringTokenizer.getTokenAt(-1), is("Ana"));
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testGetTokenAtBiggerIndex(){
        StringTokenizer stringTokenizer = new StringTokenizer();
        stringTokenizer.setString(" Ana are ,  mere   ");
        stringTokenizer.setDelimiter(",");

        assertThat(stringTokenizer.getTokenAt(90), is("Ana"));
    }

}
