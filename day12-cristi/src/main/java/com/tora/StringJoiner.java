package com.tora;

import java.util.Arrays;

public class StringJoiner {

    private String string;
    private String delimiter;

    /*Constructors*/
    public StringJoiner(String string, String delimiter) {
        this.string = string;
        this.delimiter = delimiter;
    }

    public StringJoiner(){
        this.string = "";
    }

    /*Setters*/
    public void setDelimiter(String delimiter) {
        this.delimiter = delimiter;
    }

    /*Getters*/
    public String getString() {
        return string;
    }

    public void addToken(String token){
        StringBuilder sb = new StringBuilder(string);
        if(string.length() > 0 ){
            sb.append(delimiter);
        }
        sb.append(token);

        string = sb.toString();
    }

    public void removeToken(String token){
        String[] tokens = Arrays.stream(string.split(delimiter))
                .filter(s -> !s.equals(token))
                .toArray(String[]::new);

        string = "";
        for(String t : tokens){
            addToken(t);
        }

    }

}
