package com.tora;

/**
 * Hello world!
 *
 */
public class App
{
    public static int add(int x, int y){
        if(x < 0 || y < 0){
            throw new IllegalArgumentException("Negative numbers are not allowed!");
        }

        if(x > Integer.MAX_VALUE / 2 || y > Integer.MAX_VALUE / 2){
            throw new IllegalArgumentException("Values of the parameters are too big!");
        }

        return x + y;
    }

    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
    }
}
