package com.tora;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test(expected = IllegalArgumentException.class)
    public void nonNegativeFirstParameter(){
        App.add(-10, 5);
    }

    @Test(expected = IllegalArgumentException.class)
    public void nonNegativeSecondParameter(){
        App.add(10, -5);
    }

    @Test(expected = IllegalArgumentException.class)
    public void maxValueFirstParameter(){
        App.add(Integer.MAX_VALUE / 2 + 1, 5);
    }

    @Test(expected = IllegalArgumentException.class)
    public void maxValueSecondParameter(){
        App.add(5, Integer.MAX_VALUE / 2 + 1);
    }

    @Test
    public void testSum(){
        assertThat(App.add(10, 15), is(25));
    }

}
