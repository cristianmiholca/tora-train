package com.tora;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class App {
    public static BigDecimal computeSum(List<BigDecimal> inputList) {
        return inputList.stream()
                .filter(Objects::nonNull)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public static BigDecimal computeAverage(List<BigDecimal> inputList) {
        return inputList.stream()
                .filter(Objects::nonNull)
                .reduce(BigDecimal.ZERO, BigDecimal::add)
                .divide(new BigDecimal(inputList.size()), RoundingMode.CEILING);
    }

    public static void printTenPercentBiggestNumbers(List<BigDecimal> inputList) {
        inputList.stream()
                .filter(Objects::nonNull)
                .sorted(Comparator.reverseOrder())
                .limit(Double.valueOf(Math.ceil(0.1 * inputList.size())).longValue())
                .forEach(s -> System.out.print(s + " "));
    }

}
