package com.tora;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AppTest {
    @Test
    public void testSumWithoutNullValues() {
        List<BigDecimal> list = Arrays.asList(
                BigDecimal.valueOf(5.0),
                BigDecimal.valueOf(1.2),
                BigDecimal.valueOf(3.5));
        assertThat(App.computeSum(list), is(BigDecimal.valueOf(9.7)));
    }

    @Test
    public void testSumWithNullValues() {
        List<BigDecimal> list = Arrays.asList(
                BigDecimal.valueOf(5.0),
                BigDecimal.valueOf(1.2),
                BigDecimal.valueOf(3.5),
                null);
        assertThat(App.computeSum(list), is(BigDecimal.valueOf(9.7)));
    }

    @Test
    public void testAvgWithoutNullValues() {
        List<BigDecimal> list = Arrays.asList(
                BigDecimal.valueOf(5.0),
                BigDecimal.valueOf(1.2),
                BigDecimal.valueOf(3.4));
        assertThat(App.computeAverage(list), is(BigDecimal.valueOf(3.2)));
    }

    @Test
    public void testWithoutNullValues() {
        List<BigDecimal> list = Arrays.asList(
                BigDecimal.valueOf(5.0),
                BigDecimal.valueOf(1.2),
                null,
                BigDecimal.valueOf(3.4));
        assertThat(App.computeAverage(list), is(BigDecimal.valueOf(2.4)));
    }

    @Test
    public void testPrintWithoutNullValues() {
        List<BigDecimal> list = Arrays.asList(
                BigDecimal.valueOf(5.0),
                BigDecimal.valueOf(1.2),
                BigDecimal.valueOf(3.4));

        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        App.printTenPercentBiggestNumbers(list);

        String expectedOutput = "5.0 ";
        assertThat(outContent.toString(), is(expectedOutput));
    }

    @Test
    public void testPrintWithNullValues() {
        List<BigDecimal> list = Arrays.asList(
                BigDecimal.valueOf(5.0),
                BigDecimal.valueOf(1.2),
                null,
                BigDecimal.valueOf(3.4));

        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        App.printTenPercentBiggestNumbers(list);

        String expectedOutput = "5.0 ";
        assertThat(outContent.toString(), is(expectedOutput));
    }

    @Test
    public void testSumEmptyList() {
        List<BigDecimal> list = new ArrayList<>();

        assertThat(App.computeSum(list), is(BigDecimal.ZERO));
    }

    @Test
    public void testSumNullList() {
        List<BigDecimal> list = Arrays.asList(null, null, null);

        assertThat(App.computeSum(list), is(BigDecimal.ZERO));
    }

    @Test
    public void testAverageEmptyList() {
        List<BigDecimal> list = new ArrayList<>();

        assertThat(App.computeSum(list), is(BigDecimal.ZERO));
    }

    @Test
    public void testAverageNullList() {
        List<BigDecimal> list = new ArrayList<>();

        assertThat(App.computeSum(list), is(BigDecimal.ZERO));
    }

}
